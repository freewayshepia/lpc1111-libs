/*
 * i2c.c
 *
 *  Created on: 27 août 2015
 *      Author: Olivier
 */

#ifdef __USE_CMSIS
#include "LPC11xx.h"
#endif

#include "i2c.h"

#define HIGH 1
#define LOW  0

/**
 * declaration of static variables from I2C class
 */
uint8_t I2C::addr = 0;
uint8_t I2C::status_begin_trans = 0;
uint8_t I2C::data_len =  0;
uint8_t I2C::data[128] = {0};
uint8_t I2C::data_ptr = 0;
uint8_t I2C::data_to_receive = 0;
bool I2C::is_master = 0;
void (*I2C::requestfunc)(void) = 0;
void (*I2C::receivefunc)(int) = 0;
int  (*I2C::receiveBytefunc)(int) = 0;
/**
 * End of declaration
 */


#ifdef __cplusplus /* If this is a C++ compiler, use C linkage */
extern "C" {
#endif

void I2C_IRQHandler(void){
	I2C::EventHandler();
	NVIC_ClearPendingIRQ(I2C_IRQn);
}

#ifdef __cplusplus /* If this is a C++ compiler, end C linkage */
}
#endif

inline void I2C_init(void){
    /* Note : Ce code d'initialisation à été copié à partir de : http://tinyurl.com/pk6mebf    */
    //INITIALIZE I2C pins, system components, and enable master transmit mode
    LPC_IOCON->PIO0_4         &= ~(0x303); //clear "FUNC" and "I2CMODE" bits (sec 7.4.11)
    LPC_IOCON->PIO0_4         |= 0x1;      //select I2C function SCL (sec 7.4.11)
    LPC_IOCON->PIO0_5         &= ~(0x303); //clear "FUNC" and "I2CMODE" bits (sec 7.4.12)
    LPC_IOCON->PIO0_5         |= 0x1;      //select I2C function SDA (sec 7.4.12)
    LPC_SYSCON->SYSAHBCLKCTRL |= (1<<5);   //enable clock to I2C block (sec 3.5.14)
    LPC_SYSCON->PRESETCTRL    |= (0x2);    //disable reset to I2C unit (sec 3.5.2)
}


inline void I2C_def_speed(uint32_t speed){
	LPC_I2C->SCLH = (40000000 / speed)/2;     //set clk dividers (sec 15.7.5)
	LPC_I2C->SCLL = (40000000 / speed)/2;     //set clk dividers (sec 15.7.5)
}


void I2C::begin(void){
	I2C_init();
	// entering master mode
	LPC_I2C->CONSET |= (1<<6);   //put I2C unit in master transmit mode (sec 15.8.1 and 15.7.1)
	I2C_def_speed(I2Cspeed);
	I2C::is_master = true;
	NVIC_EnableIRQ(I2C_IRQn);
}

void I2C::begin(uint8_t addr){
	I2C_init();
	LPC_I2C->ADR0 = addr;
	LPC_I2C->CONSET |= (1 << 6);
	LPC_I2C->CONSET |= (1 << 2);
	LPC_I2C->CONCLR = (1 << 3) | (1 << 4) | (1 << 5);
	is_master = false;
	NVIC_EnableIRQ(I2C_IRQn);
}
/**
 * Handler on slave mode when the device is requested data
 */
void I2C::onRequest(void (*func)(void)){
	requestfunc = func;
}
/**
 * Handler on slave mode when the device has received data
 */
void I2C::onReceive(void (*func)(int)){
	receivefunc = func;
}
/**
 * Hook on master mode, called at every byte about to be received
 * @return true : send ack after the next byte received
 * 		   false: send not ack after the next byte received
 */
void I2C::onReceiveByte(int (*func)(int)){
	receiveBytefunc = func;
}

/**
 * Fill data buffer.
 * On slave mode, the buffer is reseted just before calling onRequest()
 * On master mode, beginTransmission reset the buffer.
 */
void I2C::write(uint8_t* data, uint16_t len){
	len += data_len;
	for(int i = data_len; i < len; i++){
		I2C::data[i] = data[i];
	}
	data_len = len;
}


inline void I2C::add(uint8_t received_data){
	data[data_len++] = received_data;
}


/**
 * Request (len) byte(s) from the device with (addr) address
 */
uint16_t I2C::requestFrom(uint8_t addr, uint16_t len){
	beginTransmission(addr | 1); // LSB = 1 to Read
	data_to_receive = len;
	int dat = endTransmission();
	if(dat == 2){ // 2:received NACK on transmit of address
		return 0;
	}
	return available();
}

void I2C::beginTransmission(uint8_t addr){
	status_begin_trans = 0xff; // init status
	I2C::addr = addr;
	I2C::data_ptr = I2C::data_len = 0;
}


/**
 * This function is blocking
 * MASTER MODE Only
 * must be call after beginTransmission()
 *  0:success
	1:data too long to fit in transmit buffer
	2:received NACK on transmit of address
	3:received NACK on transmit of data
	4:other error
 */
uint8_t I2C::endTransmission(void){
	LPC_I2C->CONSET |= (1<<5); //set start bit to initiate transmission (sec 15.7.1)
	while(status_begin_trans == 0xff);
	return status_begin_trans;
}
/**
 * Return the number of byte left into the buffer
 */
uint16_t I2C::available(void){
	if((data_len - data_ptr) < 0){
		return 0;
	}
	return data_len - data_ptr;
}

/**
 * pop the firt byte from the buffer
 */
uint8_t I2C::read(void){
	if(available()){
		return data[data_ptr++];
	}
	return 0;
}

void I2C::EventHandler(void){
	switch(LPC_I2C->STAT){
		////// Master Transmitter mode
		case (0x08): // MASTER -> Started sent !
		case (0x10): // MASTER -> Repeated start sent
			LPC_I2C->CONCLR     = (1<<5);            //clear STA bit (sec 15.7.6)
			LPC_I2C->DAT = addr;
			break;
		case (0x18): // MASTER -> SLA + W, Ack has been received
		case (0x28): // MASTER -> Data transmitted, Ack has been received

			if((data_len - data_ptr) <= 0){ // no more data to be sent
				LPC_I2C->CONSET = (1<<4); // Send stop
				status_begin_trans = 0;
			}
			else{
				LPC_I2C->DAT = read();
				LPC_I2C->CONCLR = (3<<4);
			}
			break;

		case (0x20): // MASTER -> SLA + W, NOT Ack has been received
		case (0x48): // MASTER -> SLA + R, NOT Ack has been received
			status_begin_trans = 2;
			LPC_I2C->CONSET = (1<<4); // STOP Condition will be transmitted
			break;

		case (0x30): // MASTER -> data sent, not ack received
			status_begin_trans = 3;
			LPC_I2C->CONSET = (1<<4); // Send stop

			break;
		////// Master Receiver mode
		case (0x50): // Master -> Data has been received, ack returned
			add(LPC_I2C->DAT);
			data_to_receive--;
		case (0x40):
			if(receiveBytefunc != 0){ // generate hard fault if the function is executed and = 0
				if((*(receiveBytefunc))(data_to_receive)){
					LPC_I2C->CONSET = (1<<2); // send ack at the next data (continue)
				}
				else{
					LPC_I2C->CONCLR = (1<<2); // send not ack at the next data (stop)
				}
			}
			else{
				if(data_to_receive > 1){
					LPC_I2C->CONSET = (1<<2); // send ack at the next data
				}
				else{
					LPC_I2C->CONCLR = (1<<2); // send not ack at the next data
				}
			}
			break;

		case (0x38): // Master -> lost arbitration
				LPC_I2C->CONCLR = (1<<4);
				LPC_I2C->CONSET = (1<<5); // will send start bit when bus is available
			break;
		case (0x58):
				add(LPC_I2C->DAT);
				LPC_I2C->CONCLR = (1<<5); // remove start bit
				LPC_I2C->CONSET = (1<<4); // Send stop
				status_begin_trans = 0;
			break;
		////// Slave Receiver mode
		case (0x60): // SLAVE + W : Ack has been returned
			I2C::data_to_receive = 0;
			LPC_I2C->CONSET = (1<<2); // send ack after data
			break;

		case (0x80): // SLAVE + W : Data received and ack sent
			I2C::add(LPC_I2C->DAT); // retrive data
			LPC_I2C->CONSET = (1<<2); // send ack at the next data
			break;

		case (0xa0): // SLAVE + W : STOP or RESTART received
			if(I2C::receivefunc != 0){
				(*(I2C::receivefunc))(I2C::data_to_receive); // generate hard fault
			}
			I2C::data_to_receive = 0;
			LPC_I2C->CONSET = (1<<2);
			break;

		////// Slave Transmitter mode
		case (0xa8): // SLAVE + R : Ack has been returned
			data_len = data_ptr = 0;
			if(I2C::requestfunc != 0){
				(*(I2C::requestfunc))();
			}

		case (0xB8): // Data sent : Ack has been returned
			LPC_I2C->DAT = read();
			if((I2C::data_len - I2C::data_ptr) > 0){
				LPC_I2C->CONSET = (1<<2); // Data byte will be transmitted; ACK will be received
			}
			else{
				LPC_I2C->CONCLR = (1<<2);
			}
			break;

		case (0xc0): // SLAVE + R : NOT Ack has been received
			I2C::data_ptr = I2C::data_len = 0;
			LPC_I2C->CONSET = (1<<2);
			break;
		case (0xc8): // last data byte transmitted
			LPC_I2C->CONSET = (1<<2); // reactivate recognition of slave addr
			break;
	}
	LPC_I2C->CONCLR = (1<<3);
}
