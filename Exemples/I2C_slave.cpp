/*
===============================================================================
 Name        : main.c
 Author      : $(author)
 Version     :
 Copyright   : $(copyright)
 Description : main definition
===============================================================================
*/

#ifdef __USE_CMSIS
#include "LPC11xx.h"
#endif

#define I2Cspeed 100000

#include "i2c.h"
#include <cr_section_macros.h>

I2C com;
uint8_t val[3] = {0x1, 0x2, 0x3};
void requestEvent()
{
	com.write(val, 2); // respond with message of 2 bytes
						// as not expected by master
}


// TODO: insert other definitions and declarations here

int main(void) {

    // TODO: insert code here
	volatile int i;
	com.begin(0x10);
	com.onRequest(requestEvent);
    // Enter an infinite loop, just incrementing a counter
    while(1);
    return 0 ;
}
