/*
===============================================================================
 Name        : main.c
 Author      : $(author)
 Version     :
 Copyright   : $(copyright)
 Description : main definition
===============================================================================
*/

#ifdef __USE_CMSIS
#include "LPC11xx.h"
#endif

#define I2Cspeed 100000

#include "i2c.h"
#include <cr_section_macros.h>

I2C com;
uint8_t dat[5] = {0xA, 0xB, 0xC, 0xD, 0xE};


int main(void) {

    // TODO: insert code here
	volatile int i;
	com.begin();


    while(1) {
    	com.beginTransmission(0x10);
		com.write(dat, 5);
		com.endTransmission();
		com.requestFrom(0x10, 4);
		
		while (com.available()){
			char c = com.read(); // receive a byte as character
		}
		com.beginTransmission(0x20);
		com.endTransmission();
    }
    return 0 ;
}
