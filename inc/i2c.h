/*
 * i2c.h
 *
 *  Created on: 27 août 2015
 *      Author: Olivier
 */

#include <stdint.h>

#ifndef I2C_H_
#define I2C_H_

#ifndef I2Cspeed
#define I2Cspeed 400000
#endif


class I2C{
	static uint8_t addr;
	static bool is_master;

	static uint8_t data[128];

	static uint8_t data_received[16];
	static uint8_t status_begin_trans;
	static uint8_t data_len;
	static uint8_t data_ptr;
	static uint8_t data_to_receive;

public:

	void beginTransmission(uint8_t addr);
	uint8_t endTransmission(void);
	uint16_t requestFrom(uint8_t addr, uint16_t len);
	static uint16_t available(void);
	static uint8_t read(void);
	static void write(uint8_t* data, uint16_t len);
	void onRequest(void (*func)(void));
	void onReceive(void (*func)(int));
	void  onReceiveByte(int (*func)(int));
	void begin(void);
	void begin(uint8_t addr);

	static void EventHandler(void);

	static void (*requestfunc)(void);
	static void (*receivefunc)(int);
	static int  (*receiveBytefunc)(int);
	static void add(uint8_t received_data);
};

#endif /* I2C_H_ */
